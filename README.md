## Выгрузка из старой базы данных

### Предварительна подготовка
1) Сделать дамп старой базы данных

2) Развернуть (дефолтное название u4131)

3) Скачать папку со старыми изображениями в корень проекта

```bash
    # Дефолтные настройки базы данных
    adapter: 'mysql2',
    encoding: 'utf8',
    reconnect: false,
    database: 'u4131',
    pool: 5,
    host: 'localhost',
    port: 3306
 ```
## Порядок запуска задач для выгрузки из старой базы данных
```
rake unload:product_groups
rake unload:product_categories
rake unload:products
rake unload:brands
rake unload:brands_products
rake unload:country_images
rake unload:group_characteristics
rake unload:characteristics
```
