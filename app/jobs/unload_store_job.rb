class UnloadStoreJob < ActiveJob::Base
  queue_as :default

  def perform
    client = Savon.client(
      wsdl: 'http://195.64.223.8:5896/UT/ws/ws2.1cws?wsdl',
      endpoint: 'http://195.64.223.8:5896/UT/ws/ws2.1cws',
      basic_auth: ['administrator', '1']
    )

    response = client.call(:unload_store,
                xml: "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:lin='http://www.lint74.ru'>"\
                  "<soapenv:Header/>"\
                  "<soapenv:Body>"\
                  "<lin:UnloadStore>"\
                  "<lin:All>True</lin:All>"\
                  "</lin:UnloadStore>"\
                  "</soapenv:Body>"\
                  "</soapenv:Envelope>")

    items = response.body[:unload_store_response][:return][:item]

    items.each do |item|
      if item[:stores][:store].is_a? Hash
        item[:stores][:store] = [item[:stores][:store]]
      end

      item[:stores][:store].each do |store_1c|
        product = Product.find_by(article: item[:articul].split('-')[1])

        unless product.nil?
          store = Store.find_or_create_by(id_1c: store_1c[:store_id]) do |store|
            store.name = store_1c[:store_name]
            store.id_1c = store_1c[:store_id]
          end
          store.products << product unless store.products.include?(product)
          store.save
          ProductsStore.where(product_id: product.id, store_id: store.id)
            .first
            .update(quantity: store_1c[:store_count])
        end
      end
    end
  end
end
