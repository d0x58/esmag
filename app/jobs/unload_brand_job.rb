class UnloadBrandJob < ActiveJob::Base
  queue_as :default

  def perform 
    client = Savon.client(
      wsdl: 'http://195.64.223.8:5896/UT/ws/ws4.1cws?wsdl',
      endpoint: 'http://195.64.223.8:5896/UT/ws/ws4.1cws',
      basic_auth: ['administrator', '1']
    )

    response = client.call(:unload_brand).body[:unload_brand_response][:return][:brand]
    response.each do |brand|
      Brand.create(name: brand[:title])
    end
  end
end
