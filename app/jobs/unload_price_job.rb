class UnloadPriceJob < ActiveJob::Base
  queue_as :default

  def perform
    client = Savon.client(
      wsdl: 'http://195.64.223.8:5896/UT/ws/ws3.1cws?wsdl',
      endpoint: 'http://195.64.223.8:5896/UT/ws/ws3.1cws',
      basic_auth: ['administrator', '1' ],
      open_timeout: 7200,
      read_timeout: 7200,
    )

    xml_request = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:lin='http://www.lint74.ru'>"\
                  "<soapenv:Header/>"\
                  "<soapenv:Body>"\
                  "<lin:UnloadPrice>"\
                  "<lin:All>True</lin:All>"\
                  "</lin:UnloadPrice>"\
                  "</soapenv:Body>"\
                  "</soapenv:Envelope>"

    response = client.call(:unload_price, xml: xml_request)
    items = response.body[:unload_price_response][:return][:item]

    items.each do |item|
      article = item[:articul].split('-')

      product = Product.find_or_create_by(article: article[1])
      product.name = item[:title]
      product.article = article[1]

      product_category = ProductCategory.find_by(article: article[0])
      product.product_category = product_category

      prices = item[:prices]
      prices.each do |key, value|
        price_category = PriceCategory.find_by(name_1c: key)

        product_price = ProductPrice.find_by(product_id: product.id,
                                          price_category_id: price_category.id)
        product_price ||= ProductPrice.new
        product_price.price_kopecks = value
        product_price.product = product
        product_price.price_category = price_category
        product_price.save
      end
      product.save
    end
  end
end
