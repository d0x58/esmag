# == Schema Information
#
# Table name: products
#
#  id                  :integer          not null, primary key
#  name                :string(255)
#  article             :string(255)
#  description         :text(65535)
#  warranty            :text(65535)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  slug                :string(255)
#  brand_id            :integer
#  discount            :integer
#  image               :string(255)
#  product_category_id :integer
#  old_id              :integer
#
# Indexes
#
#  index_products_on_article              (article)
#  index_products_on_brand_id             (brand_id)
#  index_products_on_product_category_id  (product_category_id)
#

require 'elasticsearch/model'

class Product < ActiveRecord::Base
  # include Elasticsearch::Model
  # include Elasticsearch::Model::Callbacks

  resourcify

  paginates_per 30

  validates :name, presence: true
  validates :article, presence: true, uniqueness: true

  has_and_belongs_to_many :orders
  has_and_belongs_to_many :additional_product_groups

  has_many :stores, through: :products_stores
  has_many :products_stores
  has_many :product_prices, dependent: :delete_all
  has_many :product_characteristic_values
  accepts_nested_attributes_for :product_characteristic_values, allow_destroy: true


  belongs_to :brand
  belongs_to :product_category

  mount_uploader :image, ImageProductUploader

  def self.available user, current_subdivision
    if user.subdivisions.include? current_subdivision
      where(subdivision_id: current_subdivision)
    end
  end

  def price current_subdivision
    if current_subdivision.price_category.nil?
      nil
    else
      self.product_prices.find_by(price_category_id: current_subdivision.price_category.id)
    end
  end

  def short_description
    description.split(' ').take(80).join(' ')
  end

  def rest_of_description
    description.split(' ').drop(80).join(' ')
  end


  # def self.search(query)
  #   __elasticsearch__.search(
  #     {
  #       query: {
  #         multi_match: {
  #           query: query,
  #           fields: ['name^10', 'article']
  #         }
  #       }
  #     }
  #   )
  # end
end
# Product.__elasticsearch__.create_index!
# Product.__elasticsearch__.create_index! force: true
# Product.import
