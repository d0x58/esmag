# == Schema Information
#
# Table name: brands
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  description          :text(65535)
#  official_site        :string(255)
#  service_center       :text(65535)
#  trade_representative :text(65535)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  image                :string(255)
#  show_on_home         :boolean          default(FALSE)
#  country_id           :integer
#  old_id               :integer
#
# Indexes
#
#  index_brands_on_country_id  (country_id)
#

class Brand < ActiveRecord::Base
  mount_uploader :image, ImageBrandUploader

  belongs_to :country
  has_many :product
end
