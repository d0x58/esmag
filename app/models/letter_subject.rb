# == Schema Information
#
# Table name: letter_subjects
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  subdivision_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class LetterSubject < ActiveRecord::Base
  belongs_to :subdivision

  def self.available user, current_subdivision
    if user.subdivisions.include? current_subdivision
      where(subdivision_id: current_subdivision)
    end
  end
end
