# == Schema Information
#
# Table name: providers
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  phones         :string(255)
#  emails         :string(255)
#  contact_person :string(255)
#  address        :string(255)
#  annotation     :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Provider < ActiveRecord::Base
end
