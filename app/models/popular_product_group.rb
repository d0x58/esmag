# == Schema Information
#
# Table name: popular_product_groups
#
#  id             :integer          not null, primary key
#  order          :integer
#  active         :boolean          default(TRUE)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  subdivision_id :integer
#  name           :string(255)
#  link           :string(255)
#  image          :string(255)
#
# Indexes
#
#  index_popular_product_groups_on_subdivision_id  (subdivision_id)
#

class PopularProductGroup < ActiveRecord::Base

  belongs_to :subdivision

  mount_uploader :image, ImageUploader

  def self.available user, current_subdivision
    if user.subdivisions.include? current_subdivision
      where(subdivision_id: current_subdivision)
    end
  end

  def self.by_subdivision current_subdivision
    where(subdivision_id: current_subdivision)
  end
end
