# == Schema Information
#
# Table name: pages
#
#  id             :integer          not null, primary key
#  title          :string(255)
#  slug           :string(255)
#  announcement   :text(65535)
#  content        :text(65535)
#  title_page     :string(255)
#  key_words      :string(255)
#  description    :string(255)
#  subdivision_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Page < ActiveRecord::Base
  belongs_to :subdivision

  def url
    "/pages/#{slug}"
  end
end
