# == Schema Information
#
# Table name: price_categories
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  name_1c    :string(255)
#

class PriceCategory < ActiveRecord::Base
  has_many :product_prices
  has_many :subdivisions
end
