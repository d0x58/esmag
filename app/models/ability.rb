class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)

    if user.has_role? :project_admin
      can :manage, :all
    end

    if user.has_role? :subdivision_admin
      # can manage only his subdivision
      cannot :manage, Country
      can [:update], Subdivision.find_by(user_id: user.id)
      can :manage, ProductGroup.where(
          subdivision_id: Subdivision.find_by(user_id: user.id).id
      )
      can :manage, Client.where(
          subdivision_id: Subdivision.find_by(user_id: user.id).id
      )
    end
  end
end
