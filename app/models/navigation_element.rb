# == Schema Information
#
# Table name: navigation_elements
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  link           :string(255)
#  order          :integer          default(0)
#  subdivision_id :integer
#  position       :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class NavigationElement < ActiveRecord::Base
  belongs_to :subdivision

  enum position: {top: 0, bottom: 1}

  scope :top, -> { where(position: 0) }
  scope :bottom, -> { where(position: 1) }
end
