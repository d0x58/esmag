# == Schema Information
#
# Table name: client_feedbacks
#
#  id                :integer          not null, primary key
#  client_id         :integer
#  content           :text(65535)
#  status            :integer          default(0)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  subdivision_id    :integer
#  letter_subject_id :integer
#
# Indexes
#
#  index_client_feedbacks_on_client_id       (client_id)
#  index_client_feedbacks_on_subdivision_id  (subdivision_id)
#

class ClientFeedback < ActiveRecord::Base
  enum status: {not_read: 0, read: 1}
  belongs_to :client
  belongs_to :letter_subject
  belongs_to :subdivision

end
