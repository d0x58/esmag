# == Schema Information
#
# Table name: news
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  preview    :text(65535)
#  body       :text(65535)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  active     :boolean          default(TRUE)
#

class News < ActiveRecord::Base
  scope :active, -> { where(active: true) }
end
