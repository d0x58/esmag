# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  region_id  :integer
#
# Indexes
#
#  index_cities_on_name       (name)
#  index_cities_on_region_id  (region_id)
#

class City < ActiveRecord::Base
  has_one :subdivision
  belongs_to :region
  
end
