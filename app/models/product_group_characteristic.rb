# == Schema Information
#
# Table name: product_group_characteristics
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  order      :integer
#  active     :boolean          default(TRUE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  old_id     :integer
#

class ProductGroupCharacteristic < ActiveRecord::Base
  has_many :product_characteristics
end
