# == Schema Information
#
# Table name: product_characteristics
#
#  id                              :integer          not null, primary key
#  name                            :string(255)
#  order                           :integer
#  description                     :text(65535)
#  product_group_characteristic_id :integer
#  units                           :string(255)
#  possible_values                 :text(65535)
#  in_filter                       :boolean
#  important                       :boolean
#  active                          :boolean
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  value_type                      :integer          default(0)
#  old_id                          :integer
#

class ProductCharacteristic < ActiveRecord::Base
  belongs_to :product_group_characteristic
  has_many :product_characteristic_values

  has_many :product_categories_characteristics
  has_many :product_categories, through: :product_categories_characteristics

  enum value_type: {text: 0, numeric: 1, checkbox: 2, list: 3}

  def possible_values_to_select
    result = {}
    self.possible_values.delete("\n\r").split("\;").each do |value|
      value = value.split('=')
      result[value[0]] = value[1]
    end
    result
  end
end
