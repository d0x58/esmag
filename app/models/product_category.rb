# == Schema Information
#
# Table name: product_categories
#
#  id               :integer          not null, primary key
#  name             :string(255)
#  article          :string(255)
#  meta_tags        :text(65535)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  product_group_id :integer
#  subdivision_id   :integer
#  slug             :string(255)
#  image            :string(255)
#  old_id           :integer
#
# Indexes
#
#  index_product_categories_on_article           (article)
#  index_product_categories_on_product_group_id  (product_group_id)
#  index_product_categories_on_subdivision_id    (subdivision_id)
#

class ProductCategory < ActiveRecord::Base
  include SlugHelper

  validates :name, presence: true
  validates :article, presence: true, uniqueness: true

  belongs_to :product_group
  belongs_to :subdivision

  has_many :products

  has_many :product_categories_characteristics
  has_many :product_characteristics, through: :product_categories_characteristics

  mount_uploader :image, ImageUploader

  before_save :add_slug

  def self.available user, current_subdivision
    if user.subdivisions.include? current_subdivision
      where(subdivision_id: current_subdivision)
    end
  end

  def url
    "#{product_group.url}/product_categories/#{slug}"
  end

  private
    def add_slug
      self.slug = slug_generate self.name
    end
end
