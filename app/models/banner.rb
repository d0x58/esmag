# == Schema Information
#
# Table name: banners
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  position   :integer
#  image      :string(255)
#  link       :string(255)
#  active     :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Banner < ActiveRecord::Base
  enum position: { under_header: 0, main_slide: 1, minor_slide: 2, bottom_on_home: 3, sidebar: 4 }

  has_and_belongs_to_many :regions

  mount_uploader :image, ImageUploader
end
