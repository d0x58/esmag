# == Schema Information
#
# Table name: product_categories_characteristics
#
#  id                        :integer          not null, primary key
#  product_category_id       :integer
#  product_characteristic_id :integer
#
# Indexes
#
#  by_product_categories_characteristics  (product_category_id,product_characteristic_id) UNIQUE
#

class ProductCategoriesCharacteristic < ActiveRecord::Base
  belongs_to :product_category
  belongs_to :product_characteristic

  after_save :create_product_characteristic_values

  def create_product_characteristic_values
    self.product_category.products.each do |product|
      ProductCharacteristicValue.create(
        product_id: product.id,
        product_characteristic_id: self.product_characteristic_id
      )
    end
  end
end
