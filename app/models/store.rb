# == Schema Information
#
# Table name: stores
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  id_1c      :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_stores_on_id_1c  (id_1c)
#

class Store < ActiveRecord::Base
  has_many :products_stores
  has_many :products, through: :products_stores
end
