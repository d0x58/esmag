# == Schema Information
#
# Table name: clients
#
#  id         :integer          not null, primary key
#  first_name :string(255)
#  phone      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  email      :string(255)
#
# Indexes
#
#  index_clients_on_phone  (phone)
#

class Client < ActiveRecord::Base
  has_and_belongs_to_many :subdivisions
  has_many :client_feedbacks
  has_one :black_list

  validates_presence_of :phone

  def name
    first_name
  end

  def self.available user
    includes(:subdivisions).where(subdivisions: {id: user.subdivisions.map{|sub| sub.id}})
  end
end
