# == Schema Information
#
# Table name: black_lists
#
#  id          :integer          not null, primary key
#  client_id   :integer
#  description :text(65535)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_black_lists_on_client_id  (client_id)
#

class BlackList < ActiveRecord::Base
  belongs_to :client

  validates :phone, presence: true

  def phone= number
    if self.client
      self.client.phone = number
    else
      client = Client.find_or_create_by phone: number
      self.client = client
    end
    self.client.save
  end

  def phone
    self.client.phone if self.client
  end

end
