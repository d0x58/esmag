# == Schema Information
#
# Table name: product_characteristic_values
#
#  id                        :integer          not null, primary key
#  value_text                :text(65535)
#  value_numeric             :string(255)
#  value_checkbox            :boolean
#  value_list                :string(255)
#  product_id                :integer
#  product_characteristic_id :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#
# Indexes
#
#  by_product_characteristic_values                                  (product_id,product_characteristic_id) UNIQUE
#  index_product_characteristic_values_on_product_characteristic_id  (product_characteristic_id)
#  index_product_characteristic_values_on_product_id                 (product_id)
#

class ProductCharacteristicValue < ActiveRecord::Base
  belongs_to :product
  belongs_to :product_characteristic

  def characteristic_name
    self.product_characteristic.name
  end

  def value_by_type
    case self.product_characteristic.value_type
    when 'text'
      self.value_text
    when 'numeric'
      self.value_numeric
    when 'checkbox'
      self.value_checkbox
    when 'list'
      self.product_characteristic
          .possible_values_to_select[self.value_list]
    end
  end

  def set_value_by_type value:, type:
    case type
    when 'text'
      self.value_text = value
    when 'numeric'
      self.value_numeric = value
    when 'checkbox'
      self.value_checkbox = value
    when 'list'
      self.value_list = value
    end
  end
end
