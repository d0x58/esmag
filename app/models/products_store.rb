# == Schema Information
#
# Table name: products_stores
#
#  id         :integer          not null, primary key
#  product_id :integer
#  store_id   :integer
#  quantity   :integer
#
# Indexes
#
#  index_products_stores_on_product_id  (product_id)
#  index_products_stores_on_store_id    (store_id)
#

class ProductsStore < ActiveRecord::Base
  belongs_to :product
  belongs_to :store
end
