# encoding: utf-8

class ImageBrandUploader < CarrierWave::Uploader::Base
  include CarrierWave::RMagick

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :thumb do
    process resize_to_fit: [110, 40]
  end

  version :small do
    process resize_to_fit: [55, 20]
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end
end
