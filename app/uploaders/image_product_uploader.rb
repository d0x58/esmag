# encoding: utf-8

class ImageProductUploader < CarrierWave::Uploader::Base
  include CarrierWave::RMagick

  storage :file

  def default_url(*args)
    ActionController::Base.helpers.asset_path("product/" + [version_name, "no_image.jpg"].compact.join('_'))
  end

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :thumb do
    process resize_to_fit: [200, 200]
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end
end
