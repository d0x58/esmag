ActiveAdmin.register_page 'Integration1c' do

  menu priority: 4, label: proc{ I18n.t("active_admin.integration_1c.title") }

  action_item(:sync) do
    link_to 'Синхронизировать сайт с 1с', admin_integration1c_sync_path, method: :post
  end

  page_action :sync, method: :post do
    Thread.new do
      UnloadPriceJob.perform_later
    end

    redirect_to admin_integration1c_path
  end

  page_action :incoming_requests do

  end

  page_action :outcoming_requests do

  end
end
