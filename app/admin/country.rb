ActiveAdmin.register Country do
  permit_params :name,
                :flag

  menu parent: "Бренды"

  index do
    selectable_column
    id_column
    column :name
    column :flag, sortable: false do |country|
      image_tag country.flag.url
    end
    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :flag do
        image_tag country.flag.url
      end
      row :created_at
      row :updated_at
    end
  end


  form do |f|
    f.inputs do
      f.input :name
      f.input :flag, :as => :file, :hint => image_tag(f.object.flag.url(:thumb))
      f.input :flag_cache, :as => :hidden
    end
    f.actions
  end

end
