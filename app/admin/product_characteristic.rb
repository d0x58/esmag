ActiveAdmin.register ProductCharacteristic do
  permit_params :name,
                :order,
                :description,
                :product_group_characteristic_id,
                :units,
                :possible_values,
                :in_filter,
                :important,
                :active,
                :value_type

  menu parent: "Характеристики товаров"

  filter :name
  filter :order
  filter :description
  filter :product_group_characteristic
  filter :units
  filter :possible_values
  filter :in_filter
  filter :important
  filter :created_at
  filter :updated_at
  filter :value_type

  index do
    selectable_column
    id_column
    column :name
    column :order
    column :product_group_characteristic
    column :active
  end

  show do
    attributes_table do
      row :id
      row :name
      row :order
      row :product_group_characteristic
      row :units
      row :possible_values
      row :in_filter
      row :important
      row :value_type
      row :active
    end
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :order
      f.input :product_group_characteristic
      f.input :value_type
      f.input :units
      f.input :possible_values
      f.input :in_filter
      f.input :important
      f.input :active
    end
    f.actions
  end
end
