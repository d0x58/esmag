ActiveAdmin.register NavigationElement do
  permit_params :name,
                :link,
                :order,
                :position

  config.filters = false

  menu parent: "Меню сайта", label: 'Навигация по сайту'

  before_create do |top_menu_element|
    top_menu_element.subdivision = current_subdivision current_user
  end

  before_update do |top_menu_element|
    top_menu_element.subdivision = current_subdivision current_user
  end

  index do
    selectable_column
    column :order
    column :name
    column :link
    column :position
    actions
  end

  show do
    attributes_table do
      row :name
      row :link
      row :order
      row :subdivision
      row :position
    end
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :link
      f.input :order
      f.input :position
    end
    f.actions
  end

end
