ActiveAdmin.register ProductCategory do
  permit_params :name,
                :article,
                :meta_tags,
                :product_group_id,
                :image,
                product_characteristic_ids: []

  menu false

  config.breadcrumb = Proc.new{[]}

  controller do
    def update
      super do |format|
        redirect_to admin_catalog_path and return if resource.valid?
      end
    end

    def create
      super do |format|
        redirect_to admin_catalog_path and return if resource.valid?
      end
    end
  end

  action_item :to_catalog, only: [:show] do
    link_to 'Вернуться в каталог', admin_catalog_path
  end

  index do
    selectable_column
    id_column
    column :name
    column :product_group
    column :subdivision
    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :slug
      row :url
      row :article
      row :meta_tags
      row :product_group do
        link_to "#{product_category.product_group.name}",
                admin_product_group_path(product_category.product_group)
      end
      row :created_at
      row :updated_at
    end
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :article
      f.input :meta_tags
      f.input :product_group,
              collection: ProductGroup.all.map{|pg| ["#{pg.name}", pg.id]}
      f.input :image, as: :file, hint: image_tag(f.object.image.url(:thumb))
      f.input :image_cache, as: :hidden
      columns do
        ProductGroupCharacteristic.all.map do |pgc|
          div style: "width: 50%; float: left" do
            unless pgc.product_characteristics.empty?
              panel pgc.name do
                f.input :product_characteristics,
                        as: :check_boxes,
                        collection: pgc.product_characteristics.map{|pc| [" #{pc.name} ", pc.id]}
              end
            end
          end
        end
      end
    end
    f.actions do
      f.action :submit
      li class: "cancel" do
        link_to "Отмена", admin_catalog_path
      end
    end
  end

  member_action :products, method: [:post, :get] do
    @products = ProductCategory.find(params[:id]).products
    respond_to do |format|
      format.js
    end
  end
end
