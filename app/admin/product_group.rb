ActiveAdmin.register ProductGroup do
  permit_params :name,
                :image,
                :parent_id,
                :content

  menu false

  config.breadcrumb = Proc.new{[]}

  controller do
    def update
      super do |format|
        redirect_to admin_catalog_path and return if resource.valid?
      end
    end

    def create
      super do |format|
        redirect_to admin_catalog_path and return if resource.valid?
      end
    end
  end

  action_item :to_catalog, only: [:show] do
    link_to 'Вернуться в каталог', admin_catalog_path
  end

  index do
    selectable_column
    id_column
    column :name
    column :subdivision
    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :subdivision
      row :slug
      row :url
      row :created_at
      row :updated_at
    end
  end

  form do |f|
    f.inputs do
      f.input :name
      f.text_area :content, class: 'froala'
      f.input :parent_id, as: :select, collection: ProductGroup.available(current_user, current_subdivision(current_user)).map{|pg| ["#{pg.name}", pg.id]}
      f.input :image, as: :file, hint: image_tag(f.object.image.url(:thumb))
      f.input :image_cache, as: :hidden
    end
    f.actions do
      f.action :submit
      li class: "cancel" do
        link_to "Отмена", admin_catalog_path
      end
    end
  end
end
