ActiveAdmin.register_page "Catalog" do

  menu priority: 3, label: proc{ I18n.t("active_admin.catalog.title") }

  action_item(:new) do
    link_to 'Добавить товарную группу', new_admin_product_group_path
  end

  action_item(:new) do
    link_to 'Добавить товарную категорию', new_admin_product_category_path
  end

  action_item(:new) do
    link_to 'Добавить товар', new_admin_product_path
  end

  sidebar 'Каталог товаров' do
    render 'sidebar'
  end

  content title: proc{ I18n.t("active_admin.catalog.title") } do
    columns do
      column do
        panel 'Товары' do
          
        end
      end
    end
  end

  controller do
    def index
      @product_groups = ProductGroup.all
      unless @product_groups.empty?
        @product_groups = @product_groups.roots
      end
    end
  end
end
