ActiveAdmin.register User do
  permit_params :full_name,
                :email,
                :password,
                :password_confirmation,
                subdivision_ids: []
  
  member_action :rules, method: :get do

  end

  action_item :view, only: [:show, :edit] do
    link_to 'Права доступа', rules_admin_user_path
  end

  index do
    selectable_column
    id_column
    column :full_name
    column :email
    actions
  end

  form do |f|
    f.inputs do
      f.input :full_name
      f.input :email
      f.input :subdivisions, as: :check_boxes, collection: Subdivision.all
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

  controller do
    def update
      if params[:user][:password].blank?
        params[:user].delete "password"
        params[:user].delete "password_confirmation"
      end

      super
    end

    def rules
      @user = User.find(params[:id])
      @subdivisions = @user.subdivisions
    end
  end
end
