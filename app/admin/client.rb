ActiveAdmin.register Client do
  permit_params :first_name,
                :phone,
                subdivision_ids: []

  menu parent: "Модуль управления клиентами"

  actions :all, except: [:edit, :destroy]

  index do
    id_column
    column :first_name
    column :phone
    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :phone
      row :subdivisions do
        client.subdivisions.map{|s| s.name}.join(', ')
      end
      row :created_at
      row :updated_at
    end
  end

  form do |f|
    f.inputs do
      f.input :first_name
      f.input :phone
      f.input :subdivisions,
              as: :check_boxes,
              collection: current_user.subdivisions.map{|s| ["#{s.name}", s.id]}
    end
    f.actions
  end

end
