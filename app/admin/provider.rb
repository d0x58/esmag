ActiveAdmin.register Provider do
  permit_params :name

  index do
    selectable_column
    id_column
    column :name
    column :phones
    column :emails
    column :contact_person
    column :address
    column :annotation
    actions
  end

  

  form do |f|
    f.inputs do
      f.input :name
    end
    f.actions
  end
end
