ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    columns do
      column do
        panel "Recent Posts" do
          ul do
            
          end
        end
      end

      column do
        panel "Подразделения в которых вы состоите" do
          table_for current_user.subdivisions do
            column :id
            column :name
            column :city

            column :current do |subdivision|
              if current_subdivision(current_user).id == subdivision.id
                '+'
              end
            end

            column '' do |subdivision|
              link_to 'Выбрать', choice_admin_subdivision_path(subdivision)
            end
          end
        end
      end
    end
  end
end
