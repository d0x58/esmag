ActiveAdmin.register Subdivision do
  permit_params :name, :emails, :phones, :address, :user_id, :city_id, :main, :price_category_id

  menu parent: "Модуль управления подразделениями"


  member_action :choice, method: :get do
    session[:admin_subdivision_id] = params[:id]
    redirect_to admin_root_path
  end

  index do
    selectable_column
    id_column
    column :name
    actions
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :emails
      f.input :phones
      f.input :city
      f.input :price_category
      # f.input :users, as: :check_boxes, multiple: true, collection: User.all
      f.input :main
    end
    f.actions
  end
end
