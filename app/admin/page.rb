ActiveAdmin.register Page do
  permit_params :title,
                :slug,
                :announcement,
                :content,
                :title_page,
                :key_words,
                :description

  config.filters = false

  before_create do |page|
    page.subdivision = current_subdivision current_user
  end

  before_update do |page|
    page.subdivision = current_subdivision current_user
  end

  index do
    selectable_column
    id_column
    column :title
    column :url
    actions
  end

  show do
    panel 'Свойства' do
      attributes_table_for page do
        row :id
        row :title
        row :slug
        row :url
      end
    end

    panel 'Мета' do
      attributes_table_for page do
        row :title_page
        row :key_words
        row :description
      end
    end
  end

  form do |f|
    div(class: 'tabs') do
      ul(class: 'nav nav-tabs', role: 'tablist') do
        li { link_to 'Свойства', '#properties' }
        li { link_to 'Контент', '#content' }
        li { link_to 'Мета', '#meta' }
      end
      div(class: 'tab-content') do
        div(id: 'properties') do
          f.inputs do
            f.input :title
            f.input :slug
          end
        end
        div(id: 'content') do
          f.inputs do
            f.label :announcement
            f.text_area :announcement, class: 'froala'
            f.label :content
            f.text_area :content, class: 'froala'
          end
        end
        div(id: 'meta') do
          f.inputs do
            f.input :title_page
            f.input :key_words
            f.input :description
          end
        end
      end
    end
    f.actions
  end

end
