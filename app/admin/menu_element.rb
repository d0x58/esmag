ActiveAdmin.register MenuElement do
  permit_params :name,
                :link,
                :column,
                :order,
                :active,
                :image,
                :parent_id,
                :is_title

  menu parent: "Меню сайта", label: "Меню сайта (категории)"

  config.filters = false

  action_item(:new, only: :show) do
    if menu_element.root?
      link_to 'Создать элемент меню', new_admin_menu_element_path
    end
  end

  before_create do |menu_element|
    menu_element.subdivision = current_subdivision current_user
  end

  before_update do |menu_element|
    menu_element.subdivision = current_subdivision current_user
  end

  controller do
    def index
      scope = scoped_collection.roots.order(:order)

      @collection = scope.page() if params[:q].blank?

      respond_to do |format|
        format.html {
          render "active_admin/resource/index"
        }   
      end
    end

    def 

    def scoped_collection
      if current_user.has_role? :project_admin
        super
      else
        MenuElement.available(current_user, current_subdivision(current_user))
      end
    end
  end

  index do
    selectable_column
    column :order
    column :name
    column :subdivision
    column :active
    column :image, sortable: false do |menu_element|
      image_tag menu_element.image.url(:very_small)
    end
    actions
  end

  show do
    attributes_table do
      row :name
      row :link
      row :column
      row :parent
      row :order
      row :active
      row :is_title
      row :image do
        image_tag menu_element.image.url(:thumb)
      end
    end

    if menu_element.root?
      panel 'Дочерние элементы меню' do
        table_for menu_element.children do
          column :order
          column :name
          column :subdivision
          column :active
          column('') do |m|
            open = link_to 'Открыть', admin_menu_element_path(m)
            edit = link_to 'Изменить', edit_admin_menu_element_path(m)
            destroy = link_to 'Удалить',
                              admin_menu_element_path(m),
                              method: :delete,
                              data: { confirm: 'Вы действительно хотите удалить элемент меню?' }
            "#{open} #{edit} #{destroy}".html_safe
          end
        end
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :link
      f.input :column
      f.input :order
      f.input :active
      f.input :is_title
      f.input :parent_id,
              as: :select,
              collection: MenuElement.available(current_user, current_subdivision(current_user)).roots
      f.input :image, as: :file, hint: image_tag(f.object.image.url(:thumb))
      f.input :image_cache, as: :hidden
    end
    f.actions
  end

end
