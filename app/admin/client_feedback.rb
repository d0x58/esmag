ActiveAdmin.register ClientFeedback do
  permit_params :client_id, :content, :status

  menu parent: "Модуль управления клиентами"

  index do
    selectable_column
    id_column
    column :client
    column :content
    column :status
    actions
  end

  form do |f|
    f.inputs do
      f.input :client
      f.input :content
      f.input :status
    end
    f.actions
  end

end
