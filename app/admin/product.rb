ActiveAdmin.register Product do
  permit_params :name,
                :article,
                :description,
                :warranty,
                :brand_id,
                :discount,
                :product_category,
                product_characteristic_values_attributes: [
                  :id,
                  :value_text,
                  :value_numeric,
                  :value_checkbox,
                  :value_list
                ]

  menu false

  config.breadcrumb = Proc.new{[]}

  controller do
    def update
      super do |format|
        redirect_to admin_catalog_path and return if resource.valid?
      end
    end

    def create
      super do |format|
        redirect_to admin_catalog_path and return if resource.valid?
      end
    end
  end

  action_item :to_catalog, only: [:show] do
    link_to 'Вернуться в каталог', admin_catalog_path
  end

  index do
    selectable_column
    id_column
    column :name
    column :article
    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :article
      row :discount
      row :description
      row :product_category
      row :brand do
        brand_name = product.brand.name unless product.brand.nil?
        brand_name ||= ''
        if not product.brand.nil? and not product.brand.image.nil?
          brand_image = image_tag(product.brand.image.url(:small))
        end
        brand_image ||= ''

        "#{brand_name} #{brand_image}".html_safe
      end
      row :country do
        if not product.brand.nil? and not product.brand.country.nil?
          country_name = product.brand.country.name
        end
        country_name ||= ''

        if not product.brand.nil? and not product.brand.country.nil? and not product.brand.country.flag.nil?
          country_flag = image_tag(product.brand.country.flag.url)
        end
        country_flag ||= ''

        "#{country_name} #{country_flag}".html_safe
      end
      row :warranty
      row :image do
        image_tag product.image.url(:thumb)
      end
      row :created_at
      row :updated_at
    end

    panel 'Категории цен' do
      table_for product.product_prices do |product_price|
        column :name do |product_price|
          product_price.price_category.name
        end

        column :price do |product_price|
          product_price.price_kopecks
        end
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :article
      f.input :description
      f.input :warranty
      f.input :brand
      f.input :discount
      f.input :product_category
      f.has_many :product_characteristic_values,
                 new_record: false,
                 collection: resource.product_characteristic_values do |pcv|
        case pcv.object.product_characteristic.value_type
        when 'text'
          pcv.input :value_text, label: pcv.object.product_characteristic.name
        when 'numeric'
          pcv.input :value_numeric, label: pcv.object.product_characteristic.name
        when 'checkbox'
          pcv.input :value_checkbox, label: pcv.object.product_characteristic.name
        when 'list'
          pcv.input :value_list,
                    label: pcv.object.product_characteristic.name,
                    as: :select,
                    collection: pcv.object
                                   .product_characteristic
                                   .possible_values_to_select
                                   .map{|x| ["#{x[1]}", x[0]]}
        end
      end
    end
    f.actions do
      f.action :submit
      li class: "cancel" do
        link_to "Отмена", admin_catalog_path
      end
    end
  end
end
