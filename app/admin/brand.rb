ActiveAdmin.register Brand do
  permit_params :name,
                :description,
                :official_site,
                :service_center,
                :trade_representative,
                :image,
                :show_on_home,
                :country_id

  menu parent: "Бренды"

  filter :country
  filter :name
  filter :description
  filter :official_site
  filter :service_center
  filter :trade_representative
  filter :created_at
  filter :updated_at
  filter :show_on_home

  index do
    selectable_column
    id_column
    column :name
    column :image, sortable: false do |brand|
      image_tag brand.image.url(:small)
    end
    column :country do |brand|
      "#{brand.country.name} #{image_tag brand.country.flag.url}".html_safe if brand.country
    end
    column :show_on_home
    actions 
  end

  show do
    attributes_table do
      row :id
      row :name
      row :country
      row :description
      row :official_site
      row :service_center
      row :trade_representative
      row :show_on_home
      row :created_at
      row :updated_at
      row :image do
        image_tag brand.image.url(:thumb)
      end
    end
  end

  form html: { multipart: true } do |f|
    f.inputs do
      f.input :name
      f.input :country
      f.input :description
      f.input :official_site
      f.input :service_center
      f.input :trade_representative
      f.input :show_on_home
      f.input :image, as: :file, hint: image_tag(f.object.image.url(:thumb)) 
      f.input :image_cache, as: :hidden 
    end
    f.actions
  end
end
