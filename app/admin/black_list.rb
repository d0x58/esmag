ActiveAdmin.register BlackList do
  permit_params :client, :description, :phone

  menu parent: "Модуль управления клиентами"

  index do
    selectable_column
    id_column
    column :description
    column :client
    actions
  end

  show do
    attributes_table do
      row :id
      row :client
      row :description
      row :created_at
      row :updated_at
    end
  end

  form do |f|
    f.inputs do
      f.input :description
      f.input :phone
    end
    f.actions
  end

end
