ActiveAdmin.register PopularProductGroup do
  permit_params :name,
                :active,
                :order,
                :link,
                :image

  menu parent: "Главная страница"

  before_create do |popular_product_group|
    popular_product_group.subdivision = current_subdivision current_user
  end

  before_update do |popular_product_group|
    popular_product_group.subdivision = current_subdivision current_user
  end

  controller do
    def scoped_collection
      if current_user.has_role? :project_admin
        super
      else
        PopularProductGroup.available current_user, current_subdivision(current_user)
      end
    end
  end

  index do
    selectable_column
    id_column
    column :name
    column :order
    column :active
    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :link
      row :order
      row :active
      row :image do
        image_tag popular_product_group.image.url(:thumb)
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :link
      f.input :order
      f.input :active
      f.input :image, as: :file, hint: image_tag(f.object.image.url(:thumb))
      f.input :image_cache, as: :hidden
    end
    f.actions
  end

end
