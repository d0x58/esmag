ActiveAdmin.register AdditionalProductGroup do
  permit_params :name,
                :image

  config.filters = false

  action_item(:add_product_form, only: :show) do
    link_to 'Добавить дополнительные продукты',
            add_product_form_admin_additional_product_group_path(resource)
  end

  member_action :add_product_form, method: :get do
    @additional_product_group = AdditionalProductGroup.find(params[:id])
    @product = Product.new
  end

  member_action :add_product, method: :patch do
    @additional_product_group = AdditionalProductGroup.find(params[:id])
    @product = Product.find(params[:product][:id])
    @additional_product_group.products << @product
  end

  index do
    selectable_column
    id_column
    column :name
  end

  show do
    attributes_table do
      row :id
      row :name
      row :image
    end

    table_for resource.products do |product|
      column :id
      column(:name){ |p| link_to p.name, admin_product_path(p), target: '_blank'}
    end
  end

  form do |f|
    f.inputs do
      f.input :name
    end
    f.actions
  end
end
