ActiveAdmin.register LetterSubject do
  permit_params :name,
                :subdivision_id

  menu parent: 'Модуль управления клиентами'

  before_create do |letter_subject|
    letter_subject.subdivision = current_subdivision current_user
  end

  before_update do |letter_subject|
    letter_subject.subdivision = current_subdivision current_user
  end

  controller do
    def scoped_collection
      if current_user.has_role? :project_admin
        super
      else
        LetterSubject.available current_user, current_subdivision(current_user)
      end
    end
  end

  index do
    selectable_column
    id_column
    column :name
    column :subdivision
    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :subdivision
      row :created_at
      row :updated_at
    end
  end

  form do |f|
    f.inputs do
      f.input :name
    end
    f.actions
  end

end
