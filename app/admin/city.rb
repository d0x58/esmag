ActiveAdmin.register City do
  permit_params :name, :region_id

  menu parent: "Модуль управления подразделениями"

  index do
    selectable_column
    id_column
    column :name
    column :region
    actions 
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :region
    end
    f.actions
  end

end
