ActiveAdmin.register PriceCategory do
  permit_params :name,
                :name_1c

  filter :name
  filter :subdivisions
  filter :name_1c
  filter :created_at
  filter :updated_at
  
end
