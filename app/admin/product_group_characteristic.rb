ActiveAdmin.register ProductGroupCharacteristic do
  permit_params :name,
                :order,
                :active

  menu parent: "Характеристики товаров"

  filter :name
  filter :order
  filter :active
  filter :created_at
  filter :updated_at

  index do
    selectable_column
    id_column
    column :name
    column :order
    column :active
  end

  show do
    attributes_table do
      row :id
      row :name
      row :order
      row :active
    end
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :order
      f.input :active
    end
    f.actions
  end
end
