ActiveAdmin.register News do
  permit_params :title,
                :preview,
                :body,
                :active

  index do
    selectable_column
    id_column
    column :title
    column :active
    actions
  end

  show do
    attributes_table do
      row :id
      row :title
      row :preview
      row :body
      row :active
      row :created_at
      row :updated_at
    end
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :preview
      f.input :body
      f.input :active
    end
    f.actions
  end

end
