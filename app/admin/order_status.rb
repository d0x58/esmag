ActiveAdmin.register OrderStatus do
  permit_params :name,
                :color,
                :subject_message,
                :body_message,
                :subject_sms,
                :body_sms,
                :final_status,
                :need_date

  menu parent: "Магазин"

  index do
    selectable_column
    id_column
    column :name
    column :color
    column :subject_message
    column :body_message
    column :subject_sms
    column :body_sms
    column :final_status
    column :need_date
    actions
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :color
      f.input :subject_message
      f.input :body_message
      f.input :subject_sms
      f.input :body_sms
      f.input :final_status
      f.input :need_date
    end
    f.actions
  end

end
