ActiveAdmin.register Banner do
  permit_params :title,
                :position,
                :image,
                :link,
                :active,
                region_ids: []

  index do
    selectable_column
    id_column
    column :title
    column :position
    column :active
    actions
  end

  show do
    attributes_table do
      row :id
      row :title
      row :position
      row :link
      row :active
      row :regions do 
        banner.regions.map{|x| x.name}.join(', ')
      end
      row :created_at
      row :updated_at
    end
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :position
      f.input :link
      f.input :active
      f.input :regions, as: :check_boxes
      f.input :image, as: :file, hint: image_tag(f.object.image.url(:thumb))
      f.input :image_cache, as: :hidden
    end
    f.actions
  end

end
