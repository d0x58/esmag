ActiveAdmin.register Order do
  permit_params :client_id,
                :subdivision_id,
                :comment,
                :order_status_id

  menu parent: "Магазин"

  filter :client
  filter :subdivision
  filter :order_status
  filter :status
  filter :comment
  filter :created_at
  filter :updated_at

  index do
    selectable_column
    id_column
    column :client
    column :subdivision
    actions
  end

  show do
    attributes_table do
      row :id
      row :client
      row :subdivision
      row :status
      row :comment
      row :products do
        order.products.map{ |x| x.name }.join(',')
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :client
      f.input :subdivision
      f.input :order_status
      f.input :comment
    end
    f.actions
  end

end
