module ActiveAdmin::SubdivisionsHelper
  def current_subdivision user
    if session[:admin_subdivision_id].nil?
      user.subdivisions.first
    else
      Subdivision.find session[:admin_subdivision_id]
    end
  end
end