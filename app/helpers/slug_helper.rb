module SlugHelper
  def slug_generate string
    Translit.convert(string, enforce_language = :english).downcase.gsub ' ', '_'
  end
end