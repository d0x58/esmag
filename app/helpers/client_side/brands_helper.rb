module ClientSide
  module BrandsHelper
    def list_letters list
      list.map{|x| x[0].upcase}.uniq
    end
  end
end