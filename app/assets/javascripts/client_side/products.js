$(document).ready(function(){
  var $btnAddToBasket = $('.cgood__buy-cont .cgood__good-cart');
  var productId = $('.cgood__buy-cont input[type=hidden]').val();
  $btnAddToBasket.on('click', function(e){
    e.preventDefault();
    addToBasket(productId);
  });
})
