function addToBasket(id){
  $.ajax({
    type: 'POST',
    url: '/basket/add',
    data: {'id': id},
    beforeSend: function(xhr) {
      xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
    }
    // success: function(response){
    //   $form.removeClass('form--loading');
    //   $modal.hide();
    //   UIkit.modal('#modal-ok').show();
    // },
    // error: function(response){
    //   $form.removeClass('form--loading');
    // }
  });
}
