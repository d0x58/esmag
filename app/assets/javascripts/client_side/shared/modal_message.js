$(document).ready(function(){
  var $form_new_client_feedback = $('#new_client_feedback');

  $form_new_client_feedback.submit(function(e){
    e.preventDefault();

    var $form = $(this);
    var url = $form.attr('action');
    var data = $form.serialize();
    var $phone = $('#client_phone');
    var $modal = new $.UIkit.modal("#modal-message");


    if ($phone.val()){
      $.ajax({
        type: 'POST',
        url: url,
        data: data,
        beforeSend: function(xhr) {
          xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
          $form.addClass('form--loading');
        },
        success: function(response){
          $form.removeClass('form--loading');
          $modal.hide();
          UIkit.modal('#modal-ok').show();
        },
        error: function(response){
          $form.removeClass('form--loading');
        }
      });
    }else{
      $phone.parent().parent().addClass('has-error');
    }
  });
});