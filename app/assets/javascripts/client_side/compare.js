$(document).ready(function(){
  // catalog #product_categories page
  var $counterCompare = $('.fixed__item--compare span');
  $('.cg__good-tocompare').each(function(index){
    var $this = $(this);
    var $root = $this.parent().parent().parent();
    var id = $root.find('input').val();
    $this.on('click', function(){
      if($this.hasClass('is-active')){
        removeFromCompare(id, $this, $counterCompare);
      }else{
        addToCompare(id, $this, $counterCompare);
      };
    });
  });

  // compare
  $('.compare__table .link--red').each(function(index){
    var $this = $(this);
    var id = $this.data('id');
    $this.on('click', function(e){
      e.preventDefault();
      removeFromCompare(id);
    })
  })
})


function addToCompare(id, $button, $counterCompare){
  $.ajax({
    type: 'POST',
    url: '/compare/add',
    data: {'id': id},
    beforeSend: function(xhr) {
      xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
    },
    success: function(response){
      $button.addClass('is-active');
      $counterCompare.html(response.count);
    },
    error: function(response){
    }
  });
}

function removeFromCompare(id, $button, $counterCompare){
  $.ajax({
    type: 'POST',
    url: '/compare/remove',
    data: {'id': id},
    beforeSend: function(xhr) {
      xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
    },
    success: function(response){
      $button.removeClass('is-active');
      $counterCompare.html(response.count);
    }
    // error: function(response){
    //   $form.removeClass('form--loading');
    // }
  });
}
