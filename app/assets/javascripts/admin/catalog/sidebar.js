$(document).ready(function(){
  var $sidebar = $('.sidebar');
  $sidebar.find('li.group > span').each(function(index, element){
    $(this).on('click', function(){
      $(this).siblings('ul').toggleClass('show');
    })
  });

  $sidebar.find('li.category > a').each(function(index, element){
    $(this).on('click', function(e){
      var path = $(this).attr('href');

      var request = $.ajax({
        url: path,
        method: 'POST',
        dataType: 'html'
      });

      request.fail(function(jqXHR, textStatus){
        console.log(jqXHR, textStatus);
      })
    })
  })
})