module ClientSide
  class NewsController < BaseController
    def index 
      @news = News.active
      @last_news = News.all.active.order(:updated_at).last(3)
    end
    def show
      @news = News.active.find(params[:id])
      @last_news = News.active.order(:updated_at).last(3)
    end
  end
end
