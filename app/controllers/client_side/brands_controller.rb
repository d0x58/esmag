class ClientSide::BrandsController < ClientSide::BaseController
  def index
    @brands = Brand.all.order(:name)
  end

  def show
    @brand = Brand.find(params[:id])
  end
end
