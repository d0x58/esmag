class ClientSide::HomeController < ClientSide::BaseController
  def index
    @popular_product_groups = PopularProductGroup.by_subdivision current_subdivision
    @brands = Brand.where(show_on_home: true)
    
    @slides = []
    
    @slides << Banner.main_slide.to_a
    @slides << Banner.minor_slide.to_a.shuffle
    @slides.flatten!
  end
end
