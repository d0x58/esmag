class ClientSide::ProductGroupsController < ClientSide::BaseController
  def show
    @product_group = ProductGroup.find_by(slug: params[:slug])
    if @product_group.nil?
      raise ActiveRecord::RecordNotFound, "Товарная группа не найдена."
    end
  end
end
