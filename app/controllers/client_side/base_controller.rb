class ClientSide::BaseController < ApplicationController
  before_action :get_regions
  before_action :get_menu_elements
  before_action :get_top_menu_elements
  before_action :get_quantity_products
  before_action :get_bottom_menu_elements
  before_action :new_client_feedback
  before_action :get_basket_products
  before_action :get_banner_under_header

  helper_method :current_subdivision

  # before_action :set_location

  # def set_location
  #   if session[:location].nil?
  #     session[:location] = FreeGeoIp.geocode request.remote_ip
  #   else

  #   end
  # end

  def get_regions
    @regions = Region.includes(cities: :subdivision).all
  end

  def get_menu_elements
    @menu_elements = MenuElement.where(subdivision_id: current_subdivision&.id)
  end

  def get_top_menu_elements
    @top_menu_elements = NavigationElement.where(subdivision_id: current_subdivision&.id)
      .top
      .order(:order)
  end

  def get_bottom_menu_elements
    @bottom_menu_elements = NavigationElement.where(subdivision_id: current_subdivision&.id)
      .bottom
      .order(:order)
  end

  def get_quantity_products
    @quantity_products = Product.count
  end

  def new_client_feedback
    @client_feedback = ClientFeedback.new(subdivision_id: current_subdivision&.id)
    @letter_subjects = LetterSubject.where(subdivision_id: current_subdivision&.id)
    @client = Client.new
  end

  def get_basket_products
    @basket_products = Product.where(id: session[:basket])
  end

  def get_banner_under_header
    @banner_under_header = Banner.under_header.last
  end

  private
    def current_subdivision
      if session[:subdivision_id].nil?
        Subdivision.find_by main: 1
      else
        Subdivision.find session[:subdivision_id]
      end
    end
end
