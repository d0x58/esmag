module ClientSide
  class ClientFeedbacksController < BaseController
    def create
      @client_feedback = ClientFeedback.new(client_feedback_params)
      @client_feedback.subdivision = current_subdivision

      params[:client][:phone].gsub!(/\D/, '')
      @client = Client.find_by(phone: client_params[:phone])
      @client ||= Client.new(client_params)
      @client.save
      @client_feedback.client = @client

      respond_to do |format|
        if @client_feedback.save and @client.save
          format.json { render json: @client_feedback, status: :created }
        else
          format.json { render status: :unprocessable_entity}
        end
      end
    end

    private
      def client_params
        params.require(:client).permit(:first_name, :phone, :email)
      end

      def client_feedback_params
        params.require(:client_feedback).permit(:letter_subject_id, :content)
      end
  end
end
