class ClientSide::ProductCategoriesController < ClientSide::BaseController
  def show
    @product_category = ProductCategory.find_by(slug: params[:slug])

    @products = @product_category.products
                                 .page(params[:page])
                                 .per(params[:per])

    @product_group = @product_category.product_group
    sort_params = params[:sort_by]

    @products.order!(sort_params[0] => sort_params[1]) unless sort_params.nil?

    @last_news = News.active.order(:updated_at).last(3)
  end
end
