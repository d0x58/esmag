class ClientSide::SubdivisionsController < ClientSide::BaseController
  def choice
    session[:subdivision_id] = params[:id]
    redirect_to :back
  end
end
