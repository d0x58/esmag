class ClientSide::ProductsController < ClientSide::BaseController
  def show
    @product = Product.includes(brand: :country).find(params[:id])
    @product_category = @product.product_category
    @product_group = @product_category.product_group
    @products_history = get_products_history
    add_to_history_product product: @product
  end

  private
    def add_to_history_product product:
      if session[:product_history].nil?
        session[:product_history] = []
      end
      session[:product_history] << product.id unless session[:product_history].include?(product.id)
      if session[:product_history].length > 3
        session[:product_history] = session[:product_history].last(3)
      end
    end

    def get_products_history
      result = []
      unless session[:product_history].nil?
        session[:product_history].each do |id|
          result << Product.find(id)
        end
      end
      result.reverse
    end
end
