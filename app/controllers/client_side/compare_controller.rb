module ClientSide
  class CompareController < BaseController
    def index
      @product_groups = ProductGroup.includes(product_categories: :products)
                                    .where('products.id' => session[:compare_products])
    end

    def add
      if session[:compare_products].nil?
        session[:compare_products] = []
      end
      if params[:id].is_a? Integer or params[:id].is_a? String
        session[:compare_products] << params[:id]
      end

      respond_to do |format|
        if session[:compare_products].include?(params[:id])
          format.json{
            render json: {
              count: session[:compare_products].count
            },
            status: 200
          }
        else
          format.json{ render json: {}, status: 400 }
        end
      end
    end

    def remove
      session[:compare_products].delete(params[:id])
      respond_to do |format|
        unless session[:compare_products].include?(params[:id])
          format.json{
            render json: {
              count: session[:compare_products].count
            },
            status: 200
          }
        else
          format.json{ render json: {}, status: 400 }
        end
      end
    end
  end
end
