module ClientSide
  class OrdersController < BaseController
    def create
      params[:order][:client][:phone].gsub!(/\D/, '')
      @client = Client.find_by(phone: client_params[:phone])
      @client ||= Client.new(client_params)
      @client.save

      @products = Product.where(id: session[:basket])

      @order = Order.new
      @order.client = @client
      @order.subdivision = current_subdivision
      respond_to do |format|
        if @order.save
          format.json { render json: @order, status: :created }
        else

        end
      end
    end

    private
      def client_params
        params.require(:order)
              .require(:client)
              .permit(:first_name, :phone, :email)
      end
  end
end
