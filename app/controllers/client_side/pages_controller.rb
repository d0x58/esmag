module ClientSide
  class PagesController < BaseController
    def show
      @page = Page.find_by(slug: params[:slug])
    end
  end
end
