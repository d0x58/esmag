module ClientSide
  class BasketController < BaseController
    def index
      @products = Product.where(id: session[:basket])
      @order = Order.new
      @client = Client.new
    end

    def add
      if session[:basket].nil?
        session[:basket] = []
      end
      session[:basket] << params[:id]
    end
  end
end
