require 'net/http'

class FreeGeoIp
  HOST_NAME = 'http://freegeoip.net'

  def self.geocode ip
    query_string = HOST_NAME + "/json/#{ip}"
    uri = URI(query_string)

    JSON.parse Net::HTTP.get(uri)
  end
end