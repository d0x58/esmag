namespace :unload do
  desc 'Unload group characteristics'
  task group_characteristics: :environment do
    settings = YAML.load_file(Rails.root.join('config/unload_old_db.yml'))
    ActiveRecord::Base.establish_connection(settings['settings_db'])

    sql = 'select id, title from es_product_types where is_group = "Y"'
    group_characteristics = []

    ActiveRecord::Base.connection.execute(sql).each do |row|
      group_characteristics << [
        old_id: row[0], name: row[1]
      ]
    end

    ActiveRecord::Base.establish_connection(:development).connection

    group_characteristics.each do |group_characteristic|
      gc = ProductGroupCharacteristic.find_by(old_id: group_characteristic[0][:old_id])

      if gc.nil?
        gc = ProductGroupCharacteristic.new
      end

      gc.name = group_characteristic[0][:name]
      gc.old_id = group_characteristic[0][:old_id]
      gc.save
    end
  end
end
