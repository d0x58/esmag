namespace :unload do
  desc 'Relation brands with products'
  task brands_products: :environment do
    settings = YAML.load_file(Rails.root.join('config/unload_old_db.yml'))
    ActiveRecord::Base.establish_connection(settings['settings_db'])

    sql = 'select brand, articul from es_products'
    products = []

    ActiveRecord::Base.connection.execute(sql).each do |row|
      products << [old_brand_id: row[0], article: row[1]]
    end

    ActiveRecord::Base.establish_connection(:development).connection

    products.each do |product|
      begin
        p = Product.find_by(article: product[0][:article])
        unless p.nil?
          p.brand = Brand.find_by(old_id: product[0][:old_brand_id])
          p.save
        end
      rescue
        puts "Error #{product[0[:article]]}"
      end
    end
  end
end
