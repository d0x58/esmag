namespace :unload do
  desc 'Unload product groups'
  task product_groups: :environment do
    settings = YAML.load_file(Rails.root.join('config/unload_old_db.yml'))
    ActiveRecord::Base.establish_connection(settings['settings_db'])

    # get product groups witout parent
    sql = 'select id, title from es_product_topics where articul = "" && parent_id = 0'
    product_groups_main = []

    ActiveRecord::Base.connection.execute(sql).each do |row|
      product_groups_main << [old_id: row[0], name: row[1]]
    end

    # get product groups with parent
    sql = 'select id, title, parent_id from es_product_topics where articul = "" && parent_id != 0'
    product_groups = []

    ActiveRecord::Base.connection.execute(sql).each do |row|
      product_groups << [old_id: row[0], name: row[1], parent_id: row[2]]
    end

    ActiveRecord::Base.establish_connection(:development).connection


    # unload product groups without parent
    product_groups_main.each do |product_group|
      pg = ProductGroup.find_or_create_by(name: product_group[0][:name])
      pg.old_id = product_group[0][:old_id]

      pg.save
    end

    # unload product groups with parent
    product_groups.each do |product_group|
      pg = ProductGroup.new(name: product_group[0][:name])
      pg.old_id = product_group[0][:old_id]
      pg.parent = ProductGroup.find_by(old_id: product_group[0][:parent_id])
      pg.save
    end
  end
end
