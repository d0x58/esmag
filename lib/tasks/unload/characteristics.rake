namespace :unload do
  desc 'Unload characteristics'
  task characteristics: :environment do
    settings = YAML.load_file(Rails.root.join('config/unload_old_db.yml'))
    ActiveRecord::Base.establish_connection(settings['settings_db'])

    sql = 'select id, parent_id, title, text, type, vals, edizm from es_product_types where is_group = "N"'
    characteristics = []

    ActiveRecord::Base.connection.execute(sql).each do |row|
      characteristics << [
        old_id: row[0],
        parent_old_id: row[1],
        name: row[2],
        description: row[3],
        value_type: {
          'text' => :text,
          'int' => :numeric,
          'checkbox' => :checkbox,
          'select' => :list
        }[row[4]],
        possible_values: row[5],
        units: row[6]
      ]
    end

    ActiveRecord::Base.establish_connection(:development).connection

    characteristics.each do |characteristic|
      c = ProductCharacteristic.find_by(old_id: characteristic[0][:old_id])

      if c.nil?
        c = ProductCharacteristic.new
      end

      c.product_group_characteristic = ProductGroupCharacteristic.find_by(old_id: characteristic[0][:parent_old_id])

      c.name = characteristic[0][:name]
      c.old_id = characteristic[0][:old_id]
      c.description = characteristic[0][:description]
      c.value_type = characteristic[0][:value_type]
      c.possible_values = characteristic[0][:possible_values]
      c.units = characteristic[0][:units]
      c.save
    end
  end
end
