namespace :unload do
  desc 'Unload images of country'
  task country_images: :environment do
    settings = YAML.load_file(Rails.root.join('config/unload_old_db.yml'))
    ActiveRecord::Base.establish_connection(settings['settings_db'])

    root_path = settings['image_folder_path']

    sql = 'select * from es_images WHERE `mod`="flags"'
    countries = []

    ActiveRecord::Base.connection.execute(sql).each do |row|
      sql = "select id, name from es_countries where id = #{row[4]}"
      db_countries = ActiveRecord::Base.connection.execute(sql).to_a.flatten
      country_name = db_countries[1]
      old_id = db_countries[0]
      countries << [src: row[1], old_id: old_id, name: country_name]
    end

    ActiveRecord::Base.establish_connection(:development).connection

    countries.each do |country|
      begin
        c = Country.find_or_create_by(name: country[0][:name])
        c.old_id = country[0][:old_id]

        if File.file? "#{root_path}/images"
          File.open(File.expand_path("#{root_path}#{country[0][:src]}")) do |f|
            c.flag = f
          end
        end
        c.save
      rescue
        puts "Image not found for #{country[0][:old_id]}"
      end
    end
  end
end
