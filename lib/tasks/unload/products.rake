namespace :unload do
  desc 'Unload products'
  task products: :environment do
    settings = YAML.load_file(Rails.root.join('config/unload_old_db.yml'))
    ActiveRecord::Base.establish_connection(settings['settings_db'])

    root_path = settings['image_folder_path']

    sql = 'select articul, img, text, title, id_topic, id from es_products'
    products = []

    ActiveRecord::Base.connection.execute(sql).each do |row|
      products << [
        article: row[0],
        image: /\/images[\/\.\w]*/.match(row[1]).to_s,
        description: row[2],
        name: row[3],
        id_topic: row[4],
        old_id: row[5]
      ]
    end

    ActiveRecord::Base.establish_connection(:development).connection

    products.each do |product|
      begin
        p = Product.find_or_create_by(article: product[0][:article])
        p.description = product[0][:description]
        p.name = product[0][:name]
        p.product_category = ProductCategory.find_by(old_id: product[0][:id_topic])
        p.old_id = product[0][:old_id]

        if File.file? "#{root_path}/images"
          File.open(File.expand_path("#{root_path}#{product[0][:image]}")) do |f|
            p.image = f
          end
        end
        p.save
      rescue
        puts "Error #{p.article}"
      end
    end
  end
end
