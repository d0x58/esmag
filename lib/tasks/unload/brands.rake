namespace :unload do
  desc 'Unload brands'
  task brands: :environment do
    settings = YAML.load_file(Rails.root.join('config/unload_old_db.yml'))
    ActiveRecord::Base.establish_connection(settings['settings_db'])

    root_path = settings['image_folder_path']

    sql = 'select * from es_product_brands'
    brands = []

    ActiveRecord::Base.connection.execute(sql).each do |row|
      brands << [old_id: row[0], name: row[1], country_id: row[15]]
    end

    sql = 'select src, mod_id from es_images where `mod` = "brands"'
    brand_images = []

    ActiveRecord::Base.connection.execute(sql).each do |row|
      brand_images << [src: row[0], mod_id: row[1]]
    end

    ActiveRecord::Base.establish_connection(:development).connection

    brands.each do |brand|
      b = Brand.find_or_create_by(name: brand[0][:name])
      b.country = Country.find_by(old_id: brand[0][:country_id])
      b.old_id = brand[0][:old_id]
      b.save
    end


    brand_images.each do |brand|
      begin
        b = Brand.find_by(old_id: brand[0][:mod_id])

        if File.file? "#{root_path}/images"
          File.open(File.expand_path("#{root_path}#{brand[0][:src]}")) do |f|
            b.image = f
          end
        end

        b.save
      rescue => e
        p e
        puts "Image not found for #{brand[0][:mod_id]}"
      end
    end

  end
end
