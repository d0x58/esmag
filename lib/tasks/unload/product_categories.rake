namespace :unload do
  desc 'Unload product categories'
  task product_categories: :environment do
    settings = YAML.load_file(Rails.root.join('config/unload_old_db.yml'))
    ActiveRecord::Base.establish_connection(settings['settings_db'])

    sql = 'select articul, parent_id, title, id from es_product_topics where articul != ""'

    product_categories = []

    ActiveRecord::Base.connection.execute(sql).each do |row|
      product_categories << [
        article: row[0],
        parent_id: row[1],
        name: row[2],
        old_id: row[3]
      ]
    end

    ActiveRecord::Base.establish_connection(:development).connection

    product_categories.each do |product_category|
      pc = ProductCategory.find_or_create_by(article: product_category[0][:article])
      pc.name = product_category[0][:name]
      pc.product_group = ProductGroup.find_by(old_id: product_category[0][:parent_id])
      pc.old_id = product_category[0][:old_id]
      pc.save
    end
  end
end
