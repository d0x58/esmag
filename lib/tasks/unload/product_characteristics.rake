namespace :unload do
  desc 'Unload relations characteristics with products'
  task product_characteristic: :environment do
    settings = YAML.load_file(Rails.root.join('config/unload_old_db.yml'))
    ActiveRecord::Base.establish_connection(settings['settings_db'])

    sql = 'select id_prod, id_type, value from es_product_typeval'
    characteristic_values = []

    ActiveRecord::Base.connection.execute(sql).each do |row|
      characteristic_values << [
        product_id: row[0],
        product_characteristic_id: row[1],
        value: row[2]
      ]
    end

    ActiveRecord::Base.establish_connection(:development).connection

    characteristic_values.each do |characteristic_value|
      p = Product.find_by(old_id: characteristic_value[0][:product_id])
      pc = ProductCharacteristic.find_by(
        old_id: characteristic_value[0][:product_characteristic_id]
      )
      pcv = ProductCharacteristicValue.where(
        product_id: characteristic_value[0][:product_id],
        product_characteristic_id: characteristic_value[0][:product_characteristic_id]
      )
      if pcv.empty? and not pc.nil? and not p.nil?
        pcv = ProductCharacteristicValue.new(
          product_id: p.id,
          product_characteristic_id: pc.id
        )
        pcv.set_value_by_type(value: characteristic_value[0][:value], type: pc.value_type)
        pcv.save
      end
    end
  end
end
