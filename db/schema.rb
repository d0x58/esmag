# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161020140834) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 255
    t.text     "body",          limit: 65535
    t.string   "resource_id",   limit: 255,   null: false
    t.string   "resource_type", limit: 255,   null: false
    t.integer  "author_id",     limit: 4
    t.string   "author_type",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "additional_product_groups", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "image",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "additional_product_groups_products", force: :cascade do |t|
    t.integer "additional_product_group_id", limit: 4
    t.integer "product_id",                  limit: 4
  end

  create_table "banners", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.integer  "position",   limit: 4
    t.string   "image",      limit: 255
    t.string   "link",       limit: 255
    t.boolean  "active",                 default: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "banners_regions", force: :cascade do |t|
    t.integer "banner_id", limit: 4
    t.integer "region_id", limit: 4
  end

  create_table "black_lists", force: :cascade do |t|
    t.integer  "client_id",   limit: 4
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "black_lists", ["client_id"], name: "index_black_lists_on_client_id", using: :btree

  create_table "brands", force: :cascade do |t|
    t.string   "name",                 limit: 255
    t.text     "description",          limit: 65535
    t.string   "official_site",        limit: 255
    t.text     "service_center",       limit: 65535
    t.text     "trade_representative", limit: 65535
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.string   "image",                limit: 255
    t.boolean  "show_on_home",                       default: false
    t.integer  "country_id",           limit: 4
    t.integer  "old_id",               limit: 4
  end

  add_index "brands", ["country_id"], name: "index_brands_on_country_id", using: :btree

  create_table "cities", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "region_id",  limit: 4
  end

  add_index "cities", ["name"], name: "index_cities_on_name", using: :btree
  add_index "cities", ["region_id"], name: "index_cities_on_region_id", using: :btree

  create_table "client_feedbacks", force: :cascade do |t|
    t.integer  "client_id",         limit: 4
    t.text     "content",           limit: 65535
    t.integer  "status",            limit: 4,     default: 0
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.integer  "subdivision_id",    limit: 4
    t.integer  "letter_subject_id", limit: 4
  end

  add_index "client_feedbacks", ["client_id"], name: "index_client_feedbacks_on_client_id", using: :btree
  add_index "client_feedbacks", ["subdivision_id"], name: "index_client_feedbacks_on_subdivision_id", using: :btree

  create_table "clients", force: :cascade do |t|
    t.string   "first_name", limit: 255
    t.integer  "phone",      limit: 8
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "email",      limit: 255
  end

  add_index "clients", ["phone"], name: "index_clients_on_phone", using: :btree

  create_table "clients_subdivisions", force: :cascade do |t|
    t.integer "client_id",      limit: 4
    t.integer "subdivision_id", limit: 4
  end

  add_index "clients_subdivisions", ["client_id"], name: "index_clients_subdivisions_on_client_id", using: :btree
  add_index "clients_subdivisions", ["subdivision_id"], name: "index_clients_subdivisions_on_subdivision_id", using: :btree

  create_table "countries", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "flag",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "old_id",     limit: 4
  end

  create_table "letter_subjects", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.integer  "subdivision_id", limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "menu_elements", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.string   "link",           limit: 255
    t.integer  "column",         limit: 4,   default: 0
    t.integer  "order",          limit: 4,   default: 0
    t.boolean  "active",                     default: true
    t.string   "image",          limit: 255
    t.integer  "subdivision_id", limit: 4
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "ancestry",       limit: 255
    t.boolean  "is_title"
  end

  add_index "menu_elements", ["ancestry"], name: "index_menu_elements_on_ancestry", using: :btree

  create_table "navigation_elements", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.string   "link",           limit: 255
    t.integer  "order",          limit: 4,   default: 0
    t.integer  "subdivision_id", limit: 4
    t.integer  "position",       limit: 4
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "news", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.text     "preview",    limit: 65535
    t.text     "body",       limit: 65535
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.boolean  "active",                   default: true
  end

  create_table "order_statuses", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "color",           limit: 255
    t.string   "subject_message", limit: 255
    t.text     "body_message",    limit: 65535
    t.string   "subject_sms",     limit: 255
    t.text     "body_sms",        limit: 65535
    t.boolean  "final_status",                  default: false
    t.boolean  "need_date",                     default: false
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "client_id",       limit: 4
    t.integer  "subdivision_id",  limit: 4
    t.integer  "status",          limit: 4,     default: 0
    t.text     "comment",         limit: 65535
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.integer  "order_status_id", limit: 4
  end

  add_index "orders", ["client_id"], name: "index_orders_on_client_id", using: :btree
  add_index "orders", ["order_status_id"], name: "fk_rails_7a22cf8b0e", using: :btree
  add_index "orders", ["subdivision_id"], name: "index_orders_on_subdivision_id", using: :btree

  create_table "orders_products", force: :cascade do |t|
    t.integer "order_id",   limit: 4
    t.integer "product_id", limit: 4
  end

  add_index "orders_products", ["order_id"], name: "index_orders_products_on_order_id", using: :btree
  add_index "orders_products", ["product_id"], name: "index_orders_products_on_product_id", using: :btree

  create_table "pages", force: :cascade do |t|
    t.string   "title",          limit: 255
    t.string   "slug",           limit: 255
    t.text     "announcement",   limit: 65535
    t.text     "content",        limit: 65535
    t.string   "title_page",     limit: 255
    t.string   "key_words",      limit: 255
    t.string   "description",    limit: 255
    t.integer  "subdivision_id", limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "popular_product_groups", force: :cascade do |t|
    t.integer  "order",          limit: 4
    t.boolean  "active",                     default: true
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.integer  "subdivision_id", limit: 4
    t.string   "name",           limit: 255
    t.string   "link",           limit: 255
    t.string   "image",          limit: 255
  end

  add_index "popular_product_groups", ["subdivision_id"], name: "index_popular_product_groups_on_subdivision_id", using: :btree

  create_table "price_categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "name_1c",    limit: 255
  end

  create_table "product_categories", force: :cascade do |t|
    t.string   "name",             limit: 255
    t.string   "article",          limit: 255
    t.text     "meta_tags",        limit: 65535
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "product_group_id", limit: 4
    t.integer  "subdivision_id",   limit: 4
    t.string   "slug",             limit: 255
    t.string   "image",            limit: 255
    t.integer  "old_id",           limit: 4
  end

  add_index "product_categories", ["article"], name: "index_product_categories_on_article", using: :btree
  add_index "product_categories", ["product_group_id"], name: "index_product_categories_on_product_group_id", using: :btree
  add_index "product_categories", ["subdivision_id"], name: "index_product_categories_on_subdivision_id", using: :btree

  create_table "product_categories_characteristics", force: :cascade do |t|
    t.integer "product_category_id",       limit: 4
    t.integer "product_characteristic_id", limit: 4
  end

  add_index "product_categories_characteristics", ["product_category_id", "product_characteristic_id"], name: "by_product_categories_characteristics", unique: true, using: :btree

  create_table "product_characteristic_values", force: :cascade do |t|
    t.text     "value_text",                limit: 65535
    t.string   "value_numeric",             limit: 255
    t.boolean  "value_checkbox"
    t.string   "value_list",                limit: 255
    t.integer  "product_id",                limit: 4
    t.integer  "product_characteristic_id", limit: 4
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "product_characteristic_values", ["product_characteristic_id"], name: "index_product_characteristic_values_on_product_characteristic_id", using: :btree
  add_index "product_characteristic_values", ["product_id", "product_characteristic_id"], name: "by_product_characteristic_values", unique: true, using: :btree
  add_index "product_characteristic_values", ["product_id"], name: "index_product_characteristic_values_on_product_id", using: :btree

  create_table "product_characteristics", force: :cascade do |t|
    t.string   "name",                            limit: 255
    t.integer  "order",                           limit: 4
    t.text     "description",                     limit: 65535
    t.integer  "product_group_characteristic_id", limit: 4
    t.string   "units",                           limit: 255
    t.text     "possible_values",                 limit: 65535
    t.boolean  "in_filter"
    t.boolean  "important"
    t.boolean  "active"
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
    t.integer  "value_type",                      limit: 4,     default: 0
    t.integer  "old_id",                          limit: 4
  end

  create_table "product_group_characteristics", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "order",      limit: 4
    t.boolean  "active",                 default: true
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "old_id",     limit: 4
  end

  create_table "product_groups", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "subdivision_id", limit: 4
    t.string   "ancestry",       limit: 255
    t.string   "slug",           limit: 255
    t.string   "image",          limit: 255
    t.text     "content",        limit: 65535
    t.integer  "old_id",         limit: 4
  end

  add_index "product_groups", ["ancestry"], name: "index_product_groups_on_ancestry", using: :btree
  add_index "product_groups", ["subdivision_id"], name: "fk_rails_adb6b83feb", using: :btree

  create_table "product_prices", force: :cascade do |t|
    t.integer "price_category_id", limit: 4
    t.integer "price_kopecks",     limit: 4,   default: 0,     null: false
    t.string  "price_currency",    limit: 255, default: "RUB", null: false
    t.integer "product_id",        limit: 4
    t.boolean "is_master",                     default: false
  end

  add_index "product_prices", ["price_category_id"], name: "index_product_prices_on_price_category_id", using: :btree
  add_index "product_prices", ["product_id", "price_category_id"], name: "index_product_prices_on_product_id_and_price_category_id", unique: true, using: :btree
  add_index "product_prices", ["product_id"], name: "index_product_prices_on_product_id", using: :btree

  create_table "products", force: :cascade do |t|
    t.string   "name",                limit: 255
    t.string   "article",             limit: 255
    t.text     "description",         limit: 65535
    t.text     "warranty",            limit: 65535
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "slug",                limit: 255
    t.integer  "brand_id",            limit: 4
    t.integer  "discount",            limit: 4
    t.string   "image",               limit: 255
    t.integer  "product_category_id", limit: 4
    t.integer  "old_id",              limit: 4
  end

  add_index "products", ["article"], name: "index_products_on_article", using: :btree
  add_index "products", ["brand_id"], name: "index_products_on_brand_id", using: :btree
  add_index "products", ["product_category_id"], name: "index_products_on_product_category_id", using: :btree

  create_table "products_stores", force: :cascade do |t|
    t.integer "product_id", limit: 4
    t.integer "store_id",   limit: 4
    t.integer "quantity",   limit: 4
  end

  add_index "products_stores", ["product_id"], name: "index_products_stores_on_product_id", using: :btree
  add_index "products_stores", ["store_id"], name: "index_products_stores_on_store_id", using: :btree

  create_table "providers", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.string   "phones",         limit: 255
    t.string   "emails",         limit: 255
    t.string   "contact_person", limit: 255
    t.string   "address",        limit: 255
    t.string   "annotation",     limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "regions", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.integer  "resource_id",   limit: 4
    t.string   "resource_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "stores", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "id_1c",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "stores", ["id_1c"], name: "index_stores_on_id_1c", using: :btree

  create_table "subdivisions", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.string   "emails",            limit: 255, default: "--- []\n"
    t.string   "phones",            limit: 255, default: "--- []\n"
    t.string   "address",           limit: 255
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.integer  "city_id",           limit: 4
    t.boolean  "main",                          default: false,      null: false
    t.integer  "price_category_id", limit: 4
  end

  add_index "subdivisions", ["city_id"], name: "index_subdivisions_on_city_id", using: :btree
  add_index "subdivisions", ["price_category_id"], name: "index_subdivisions_on_price_category_id", using: :btree

  create_table "subdivisions_users", force: :cascade do |t|
    t.integer "user_id",        limit: 4
    t.integer "subdivision_id", limit: 4
  end

  add_index "subdivisions_users", ["subdivision_id"], name: "index_subdivisions_users_on_subdivision_id", using: :btree
  add_index "subdivisions_users", ["user_id"], name: "index_subdivisions_users_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "full_name",              limit: 255
    t.string   "position",               limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.integer "role_id", limit: 4
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  add_foreign_key "orders", "order_statuses"
  add_foreign_key "product_groups", "subdivisions"
end
