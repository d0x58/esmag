class RemoveDateFromProductPrices < ActiveRecord::Migration
  def change
    remove_column :product_prices, :created_at
    remove_column :product_prices, :updated_at
  end
end
