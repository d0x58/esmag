class CreateProductCharacteristicValues < ActiveRecord::Migration
  def change
    create_table :product_characteristic_values do |t|
      t.string :value_text
      t.string :value_numeric
      t.boolean :value_checkbox
      t.string :value_list
      t.belongs_to :product, index: true
      t.belongs_to :product_characteristic, index: true

      t.timestamps null: false
    end
  end
end
