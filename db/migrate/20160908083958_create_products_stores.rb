class CreateProductsStores < ActiveRecord::Migration
  def change
    create_table :products_stores do |t|
      t.belongs_to :product, index: true
      t.belongs_to :store, index: true
      t.integer :quantity
    end
  end
end
