class CreateNavigationElements < ActiveRecord::Migration
  def change
    create_table :navigation_elements do |t|
      t.string :name
      t.string :link
      t.integer :order, default: 0
      t.belongs_to :subdivision
      t.integer :position
      
      t.timestamps null: false
    end
  end
end
