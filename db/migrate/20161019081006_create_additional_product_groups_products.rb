class CreateAdditionalProductGroupsProducts < ActiveRecord::Migration
  def change
    create_table :additional_product_groups_products do |t|
      t.belongs_to :additional_product_group
      t.belongs_to :product
    end
  end
end
