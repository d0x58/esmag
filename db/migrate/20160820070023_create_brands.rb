class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.string :name
      t.text :description
      t.string :official_site
      t.text :service_center
      t.text :trade_representative
      

      t.timestamps null: false
    end
  end
end
