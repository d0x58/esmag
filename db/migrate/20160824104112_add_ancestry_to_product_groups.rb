class AddAncestryToProductGroups < ActiveRecord::Migration
  def change
    add_column :product_groups, :ancestry, :string
    add_index :product_groups, :ancestry
  end
end
