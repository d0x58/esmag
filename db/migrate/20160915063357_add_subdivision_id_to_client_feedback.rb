class AddSubdivisionIdToClientFeedback < ActiveRecord::Migration
  def change
    add_reference :client_feedbacks, :subdivision, index: true
  end
end
