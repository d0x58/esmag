class AddIndexToProductCharacteristicValues < ActiveRecord::Migration
  def change
    add_index :product_characteristic_values,
              [:product_id, :product_characteristic_id],
              unique: true,
              name: 'by_product_characteristic_values'
  end
end
