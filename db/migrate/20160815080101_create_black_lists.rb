class CreateBlackLists < ActiveRecord::Migration
  def change
    create_table :black_lists do |t|
      t.belongs_to :client, index: true
      t.text :description

      t.timestamps null: false
    end
  end
end
