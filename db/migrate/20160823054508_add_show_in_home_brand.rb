class AddShowInHomeBrand < ActiveRecord::Migration
  def change
    add_column :brands, :show_on_home, :boolean, default: false
  end
end
