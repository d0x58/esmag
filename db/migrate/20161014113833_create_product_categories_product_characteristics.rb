class CreateProductCategoriesProductCharacteristics < ActiveRecord::Migration
  def change
    create_table :product_categories_characteristics do |t|
      t.belongs_to :product_category
      t.belongs_to :product_characteristic
    end
  end
end
