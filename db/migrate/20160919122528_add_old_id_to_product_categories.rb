class AddOldIdToProductCategories < ActiveRecord::Migration
  def change
    add_column :product_categories, :old_id, :integer, unique: true
  end
end
