class AddIndexToProductCategoriesProductCharacteristics < ActiveRecord::Migration
  def change
    add_index :product_categories_characteristics,
              [:product_category_id, :product_characteristic_id],
              unique: true,
              name: 'by_product_categories_characteristics'
  end
end
