class AddOldIdToProductGroups < ActiveRecord::Migration
  def change
    add_column :product_groups, :old_id, :integer, unique: true
  end
end
