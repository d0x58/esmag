class AddOldIdToCountry < ActiveRecord::Migration
  def change
    add_column :countries, :old_id, :integer, unique: true
  end
end
