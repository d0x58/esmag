class AddSubdivisionIdToPopularProductGroup < ActiveRecord::Migration
  def change
    add_reference :popular_product_groups, :subdivision, index: true
  end
end
