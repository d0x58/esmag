class AddOldIdToProduct < ActiveRecord::Migration
  def change
    add_column :products, :old_id, :integer, index: true, unique: true
  end
end
