class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :article, index: true, unique: true
      t.text :description
      t.text :warranty

      t.timestamps null: false
    end
  end
end
