class CreateProductPrices < ActiveRecord::Migration
  def change
    create_table :product_prices do |t|
      t.belongs_to :price_category, index: true
      t.money :price

      t.timestamps null: false
    end
  end
end
