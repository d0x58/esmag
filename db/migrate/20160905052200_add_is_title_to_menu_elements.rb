class AddIsTitleToMenuElements < ActiveRecord::Migration
  def change
    add_column :menu_elements, :is_title, :boolean
  end
end
