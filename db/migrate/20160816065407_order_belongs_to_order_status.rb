class OrderBelongsToOrderStatus < ActiveRecord::Migration
  def change
    add_reference :orders, :order_status, foreign_key: true
  end
end
