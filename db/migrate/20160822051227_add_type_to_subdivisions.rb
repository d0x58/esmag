class AddTypeToSubdivisions < ActiveRecord::Migration
  def change
    add_column :subdivisions, :main, :boolean, default: false, null: false
  end
end
