class CreateBannersRegions < ActiveRecord::Migration
  def change
    create_table :banners_regions do |t|
      t.belongs_to :banner
      t.belongs_to :region
    end
  end
end
