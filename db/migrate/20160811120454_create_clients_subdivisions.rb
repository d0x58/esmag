class CreateClientsSubdivisions < ActiveRecord::Migration
  def change
    create_table :clients_subdivisions do |t|
      t.belongs_to :client, index: true
      t.belongs_to :subdivision, index: true
    end
  end
end
