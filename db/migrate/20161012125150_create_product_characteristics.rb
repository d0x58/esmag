class CreateProductCharacteristics < ActiveRecord::Migration
  def change
    create_table :product_characteristics do |t|
      t.string :name
      t.integer :order
      t.text :description
      t.belongs_to :product_group_characteristic
      t.string :units
      t.text :possible_values
      t.boolean :in_filter
      t.boolean :important
      t.boolean :active
      
      t.timestamps null: false
    end
  end
end
