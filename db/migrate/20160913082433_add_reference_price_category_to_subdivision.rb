class AddReferencePriceCategoryToSubdivision < ActiveRecord::Migration
  def change
    add_reference :subdivisions, :price_category, index: true
  end
end
