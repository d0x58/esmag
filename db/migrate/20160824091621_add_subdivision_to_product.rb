class AddSubdivisionToProduct < ActiveRecord::Migration
  def change
    add_reference :products, :subdivision, index: true
  end
end
