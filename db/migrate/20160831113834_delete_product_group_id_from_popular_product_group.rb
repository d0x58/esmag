class DeleteProductGroupIdFromPopularProductGroup < ActiveRecord::Migration
  def change
    remove_column :popular_product_groups, :product_group_id
  end
end
