class CreateUsersSubdivisions < ActiveRecord::Migration
  def change
    create_table :subdivisions_users do |t|
      t.belongs_to :user, index: true
      t.belongs_to :subdivision, index: true
    end
  end
end
