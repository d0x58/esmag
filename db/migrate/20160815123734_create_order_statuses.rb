class CreateOrderStatuses < ActiveRecord::Migration
  def change
    create_table :order_statuses do |t|
      t.string :name
      t.string :color
      t.string :subject_message
      t.text :body_message
      t.string :subject_sms
      t.text :body_sms
      t.boolean :final_status, default: 0
      t.boolean :need_date, default: 0

      t.timestamps null: false
    end
  end
end