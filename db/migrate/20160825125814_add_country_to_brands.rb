class AddCountryToBrands < ActiveRecord::Migration
  def change
    add_reference :brands, :country, index: true
  end
end
