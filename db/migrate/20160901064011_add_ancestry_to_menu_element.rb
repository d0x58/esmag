class AddAncestryToMenuElement < ActiveRecord::Migration
  def change
    add_column :menu_elements, :ancestry, :string
    add_index :menu_elements, :ancestry
  end
end
