class RelationsSubdivisionHasManyProductGroups < ActiveRecord::Migration
  def change
    add_reference :product_groups, :subdivision, foreign_key: true
  end
end
