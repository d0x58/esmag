class AddSlug < ActiveRecord::Migration
  def change
    add_column :product_groups, :slug, :string, index: true
    add_column :product_categories, :slug, :string, index: true
    add_column :products, :slug, :string, index: true
  end
end
