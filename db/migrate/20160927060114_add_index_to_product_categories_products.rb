class AddIndexToProductCategoriesProducts < ActiveRecord::Migration
  def change
    add_index :product_categories_products, [ :product_category_id, :product_id ], unique: true, name: 'by_product_categories_and_products'
  end
end
