class CreateProductCategories < ActiveRecord::Migration
  def change
    create_table :product_categories do |t|
      t.string :name
      t.string :article, index: true, unique: true
      t.text :meta_tags

      t.timestamps null: false
    end
  end
end
