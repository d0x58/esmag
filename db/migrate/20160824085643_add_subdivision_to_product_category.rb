class AddSubdivisionToProductCategory < ActiveRecord::Migration
  def change
    add_reference :product_categories, :subdivision, index: true
  end
end
