class CreatePopularProductCategories < ActiveRecord::Migration
  def change
    create_table :popular_product_categories do |t|
      t.belongs_to :product_category, index: true
      t.integer :order
      t.boolean :active, default: true
      t.timestamps null: false
    end
  end
end
