class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.string :slug
      t.text :announcement
      t.text :content
      t.string :title_page
      t.string :key_words
      t.string :description
      t.belongs_to :subdivision
      
      t.timestamps null: false
    end
  end
end
