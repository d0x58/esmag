class DropPopularProductCategory < ActiveRecord::Migration
  def change
    drop_table :popular_product_categories
  end
end
