class CreateProviders < ActiveRecord::Migration
  def change
    create_table :providers do |t|
      t.string :name
      t.string :phones
      t.string :emails
      t.string :contact_person
      t.string :address
      t.string :annotation
      
      t.timestamps null: false
    end
  end
end
