class ProductPriceBelongsToProduct < ActiveRecord::Migration
  def change
    add_reference :product_prices, :product, index: true
  end
end
