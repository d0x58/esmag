class RemoveMoneyFromProduct < ActiveRecord::Migration
  def change
    remove_money :products, :price
  end
end
