class ChangeValueTextOfProductCharacteristicValue < ActiveRecord::Migration
  def change
    change_column :product_characteristic_values, :value_text, :text
  end
end
