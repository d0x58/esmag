class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.string :name
      t.string :id_1c, index: true, unique: true

      t.timestamps null: false
    end
  end
end
