class CreateSubdivisions < ActiveRecord::Migration
  def change
    create_table :subdivisions do |t|
      t.string :name
      t.string :emails, array: true, default: []
      t.string :phones, array: true, default: []
      t.string :address
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
  end
end
