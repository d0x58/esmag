class AddReferenceClientFeedbackToLetterSubject < ActiveRecord::Migration
  def change
    add_reference :client_feedbacks, :letter_subject
  end
end
