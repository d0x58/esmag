class AddTypeToProductCharacteristics < ActiveRecord::Migration
  def change
    add_column :product_characteristics, :value_type, :integer, default: 0
  end
end
