class AddFieldsToPopularProductGroups < ActiveRecord::Migration
  def change
    add_column :popular_product_groups, :name, :string
    add_column :popular_product_groups, :link, :string
    add_column :popular_product_groups, :image, :string
  end
end
