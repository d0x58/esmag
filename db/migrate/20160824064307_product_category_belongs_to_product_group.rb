class ProductCategoryBelongsToProductGroup < ActiveRecord::Migration
  def change
    add_reference :product_categories, :product_group, index: true
  end
end
