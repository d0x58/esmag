class CreateProductGroupCharacteristics < ActiveRecord::Migration
  def change
    create_table :product_group_characteristics do |t|
      t.string :name
      t.integer :order
      t.boolean :active, default: true
      
      t.timestamps null: false
    end
  end
end
