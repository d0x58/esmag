class RemoveUserIdFromSubdivision < ActiveRecord::Migration
  def change
    remove_column :subdivisions, :user_id, :integer, index: true
  end
end
