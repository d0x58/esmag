class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.belongs_to :client, index: true
      t.belongs_to :subdivision, index: true
      t.integer :status, default: 0
      t.text :comment

      t.timestamps null: false
    end
  end
end
