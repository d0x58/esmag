class AddOldIdToProductCharacteristics < ActiveRecord::Migration
  def change
    add_column :product_characteristics, :old_id, :integer, unique: true, index: true
  end
end
