class AddIndexToProductPrices < ActiveRecord::Migration
  def change
    add_index :product_prices,
              [:product_id, :price_category_id],
              unique: true
  end
end
