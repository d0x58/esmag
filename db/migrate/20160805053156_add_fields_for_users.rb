class AddFieldsForUsers < ActiveRecord::Migration
  def change
    add_column :users, :full_name, :string
    add_column :users, :position, :string
  end
end
