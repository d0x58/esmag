class RemoveSubdivisionIdFromProduct < ActiveRecord::Migration
  def change
    remove_reference :products, :subdivision, index: true
  end
end
