class AddImageToProductGroup < ActiveRecord::Migration
  def change
    add_column :product_groups, :image, :string
  end
end
