class AddOldIdToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :old_id, :integer, unique: true
  end
end
