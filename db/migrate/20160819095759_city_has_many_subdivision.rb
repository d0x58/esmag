class CityHasManySubdivision < ActiveRecord::Migration
  def change
    add_reference :subdivisions, :city, index: true
  end
end
