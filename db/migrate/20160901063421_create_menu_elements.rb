class CreateMenuElements < ActiveRecord::Migration
  def change
    create_table :menu_elements do |t|
      t.string :name
      t.string :link
      t.integer :column, default: 0
      t.integer :order, default: 0
      t.boolean :active, default: true
      t.string :image
      t.belongs_to :subdivision
      t.timestamps null: false
    end
  end
end
