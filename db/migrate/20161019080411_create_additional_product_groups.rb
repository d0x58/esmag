class CreateAdditionalProductGroups < ActiveRecord::Migration
  def change
    create_table :additional_product_groups do |t|
      t.string :name
      t.string :image
      
      t.timestamps null: false
    end
  end
end
