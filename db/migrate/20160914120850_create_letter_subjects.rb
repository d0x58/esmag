class CreateLetterSubjects < ActiveRecord::Migration
  def change
    create_table :letter_subjects do |t|
      t.string :name
      t.belongs_to :subdivision
      
      t.timestamps null: false
    end
  end
end
