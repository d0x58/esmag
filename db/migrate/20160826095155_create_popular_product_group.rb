class CreatePopularProductGroup < ActiveRecord::Migration
  def change
    create_table :popular_product_groups do |t|
      t.belongs_to :product_group, index: true
      t.integer :order
      t.boolean :active, default: true
      t.timestamps null: false
    end
  end
end
