class AddIsMasterToProductPrice < ActiveRecord::Migration
  def change
    add_column :product_prices, :is_master, :boolean, default: false
  end
end
