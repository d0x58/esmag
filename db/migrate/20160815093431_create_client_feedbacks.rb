class CreateClientFeedbacks < ActiveRecord::Migration
  def change
    create_table :client_feedbacks do |t|
      t.belongs_to :client, index: true
      t.text :content
      t.integer :status, default: 0

      t.timestamps null: false
    end
  end
end
