class AddOldIdToProductGroupCharacteristic < ActiveRecord::Migration
  def change
    add_column :product_group_characteristics, :old_id, :integer, unique: true, index: true
  end
end
