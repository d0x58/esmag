if defined? Bullet
  Bullet.enable = true
  # Bullet.alert = true
  Bullet.console = true
  Bullet.add_footer = true
end
