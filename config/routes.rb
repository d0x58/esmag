Rails.application.routes.draw do

  devise_for :users, ActiveAdmin::Devise.config

  ActiveAdmin.routes(self)

  scope module: 'client_side' do
    root to: 'home#index'
    resources :brands, only: [:index, :show]

    resources :subdivisions, only: [] do
      collection do
        get ':id/choice', to: 'subdivisions#choice'
      end
    end
    resources :product_groups, only: [:show], param: :slug do
      resources :product_categories, only: [:show], param: :slug do
      end
    end
    resources :products, only: [:show]
    resources :pages, only: [:show], param: :slug
    resources :news, only: [:index, :show]
    resources :client_feedbacks, only: [:create]

    # compare controller
    get 'compare', to: 'compare#index'
    post 'compare/add', to: 'compare#add'
    post 'compare/remove', to: 'compare#remove'

    # basket controller
    get 'basket', to: 'basket#index'
    post 'basket/add', to: 'basket#add'

    # orders controller
    resources :orders, only: [:create]

    get 'search', to: 'search#search'
  end
end
