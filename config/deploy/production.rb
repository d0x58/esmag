set :port, 22
set :user, 'deployer'
set :password, 'Caardistrymark1234'
set :deploy_via, :remote_cache
set :use_sudo, false

server '91.226.80.209',
  roles: [:web, :app, :db],
  port: fetch(:port),
  user: fetch(:user),
  password: fetch(:password),
  primary: true

set :deploy_to, "/home/#{fetch(:user)}/apps/#{fetch(:application)}"

set :ssh_options, {
  forward_agent: true,
  auth_methods: %w(publickey),
  user: 'deployer',
}

set :rails_env, :production
set :conditionally_migrate, true 