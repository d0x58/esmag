# == Schema Information
#
# Table name: order_statuses
#
#  id              :integer          not null, primary key
#  name            :string(255)
#  color           :string(255)
#  subject_message :string(255)
#  body_message    :text(65535)
#  subject_sms     :string(255)
#  body_sms        :text(65535)
#  final_status    :boolean          default(FALSE)
#  need_date       :boolean          default(FALSE)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

require 'rails_helper'

RSpec.describe OrderStatus, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
