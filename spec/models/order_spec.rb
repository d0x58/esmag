# == Schema Information
#
# Table name: orders
#
#  id              :integer          not null, primary key
#  client_id       :integer
#  subdivision_id  :integer
#  status          :integer          default(0)
#  comment         :text(65535)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  order_status_id :integer
#
# Indexes
#
#  fk_rails_7a22cf8b0e             (order_status_id)
#  index_orders_on_client_id       (client_id)
#  index_orders_on_subdivision_id  (subdivision_id)
#
# Foreign Keys
#
#  fk_rails_7a22cf8b0e  (order_status_id => order_statuses.id)
#

require 'rails_helper'

RSpec.describe Order, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
