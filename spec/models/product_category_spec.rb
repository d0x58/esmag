# == Schema Information
#
# Table name: product_categories
#
#  id               :integer          not null, primary key
#  name             :string(255)
#  article          :string(255)
#  meta_tags        :text(65535)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  product_group_id :integer
#  subdivision_id   :integer
#  slug             :string(255)
#  image            :string(255)
#  old_id           :integer
#
# Indexes
#
#  index_product_categories_on_article           (article)
#  index_product_categories_on_product_group_id  (product_group_id)
#  index_product_categories_on_subdivision_id    (subdivision_id)
#

require 'rails_helper'

RSpec.describe ProductCategory, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
