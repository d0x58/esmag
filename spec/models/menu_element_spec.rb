# == Schema Information
#
# Table name: menu_elements
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  link           :string(255)
#  column         :integer          default(0)
#  order          :integer          default(0)
#  active         :boolean          default(TRUE)
#  image          :string(255)
#  subdivision_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  ancestry       :string(255)
#  is_title       :boolean
#
# Indexes
#
#  index_menu_elements_on_ancestry  (ancestry)
#

require 'rails_helper'

RSpec.describe MenuElement, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
