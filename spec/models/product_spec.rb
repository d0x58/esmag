# == Schema Information
#
# Table name: products
#
#  id                  :integer          not null, primary key
#  name                :string(255)
#  article             :string(255)
#  description         :text(65535)
#  warranty            :text(65535)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  slug                :string(255)
#  brand_id            :integer
#  discount            :integer
#  image               :string(255)
#  product_category_id :integer
#  old_id              :integer
#
# Indexes
#
#  index_products_on_article              (article)
#  index_products_on_brand_id             (brand_id)
#  index_products_on_product_category_id  (product_category_id)
#

require 'rails_helper'

RSpec.describe Product, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
