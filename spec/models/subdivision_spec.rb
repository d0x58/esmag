# == Schema Information
#
# Table name: subdivisions
#
#  id                :integer          not null, primary key
#  name              :string(255)
#  emails            :string(255)      default("--- []\n")
#  phones            :string(255)      default("--- []\n")
#  address           :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  city_id           :integer
#  main              :boolean          default(FALSE), not null
#  price_category_id :integer
#
# Indexes
#
#  index_subdivisions_on_city_id            (city_id)
#  index_subdivisions_on_price_category_id  (price_category_id)
#

require 'rails_helper'

RSpec.describe Subdivision, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
