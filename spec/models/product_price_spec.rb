# == Schema Information
#
# Table name: product_prices
#
#  id                :integer          not null, primary key
#  price_category_id :integer
#  price_kopecks     :integer          default(0), not null
#  price_currency    :string(255)      default("RUB"), not null
#  product_id        :integer
#  is_master         :boolean          default(FALSE)
#
# Indexes
#
#  index_product_prices_on_price_category_id                 (price_category_id)
#  index_product_prices_on_product_id                        (product_id)
#  index_product_prices_on_product_id_and_price_category_id  (product_id,price_category_id) UNIQUE
#

require 'rails_helper'

RSpec.describe ProductPrice, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
