# == Schema Information
#
# Table name: product_characteristics
#
#  id                              :integer          not null, primary key
#  name                            :string(255)
#  order                           :integer
#  description                     :text(65535)
#  product_group_characteristic_id :integer
#  units                           :string(255)
#  possible_values                 :text(65535)
#  in_filter                       :boolean
#  important                       :boolean
#  active                          :boolean
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  value_type                      :integer          default(0)
#  old_id                          :integer
#

FactoryGirl.define do
  factory :product_characteristic do
    
  end
end
