# == Schema Information
#
# Table name: product_characteristic_values
#
#  id                        :integer          not null, primary key
#  value_text                :text(65535)
#  value_numeric             :string(255)
#  value_checkbox            :boolean
#  value_list                :string(255)
#  product_id                :integer
#  product_characteristic_id :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#
# Indexes
#
#  by_product_characteristic_values                                  (product_id,product_characteristic_id) UNIQUE
#  index_product_characteristic_values_on_product_characteristic_id  (product_characteristic_id)
#  index_product_characteristic_values_on_product_id                 (product_id)
#

FactoryGirl.define do
  factory :product_characteristic_value do
    
  end
end
