# == Schema Information
#
# Table name: clients
#
#  id         :integer          not null, primary key
#  first_name :string(255)
#  phone      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  email      :string(255)
#
# Indexes
#
#  index_clients_on_phone  (phone)
#

FactoryGirl.define do
  factory :client do
    
  end
end
