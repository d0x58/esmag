# == Schema Information
#
# Table name: product_groups
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  subdivision_id :integer
#  ancestry       :string(255)
#  slug           :string(255)
#  image          :string(255)
#  content        :text(65535)
#  old_id         :integer
#
# Indexes
#
#  fk_rails_adb6b83feb               (subdivision_id)
#  index_product_groups_on_ancestry  (ancestry)
#
# Foreign Keys
#
#  fk_rails_adb6b83feb  (subdivision_id => subdivisions.id)
#

FactoryGirl.define do
  factory :product_group do
    
  end
end
